using System;
using System.Collections;
using IMIRT.SaveSystem;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class SaveSystemTests
{
    [UnityTest]
    public IEnumerator TestSaveLoadDefault()
    {
        GameObject testObject = new GameObject("Test Save System");
        testObject.AddComponent<DefaultSaveSystemFactory>();
        SaveSystem saveSystem = testObject.AddComponent<SaveSystem>();

        MockSaveData data = new MockSaveData();
        saveSystem.Save(data);
        MockSaveData loadedData = saveSystem.Load<MockSaveData>();
        
        yield return null;
        
        Assert.AreEqual(data.Name, loadedData.Name);
    }

}

[Serializable]
public class MockSaveData
{
    public string Name => "Mock Data";
}
