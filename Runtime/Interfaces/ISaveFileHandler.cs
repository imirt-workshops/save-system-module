﻿namespace IMIRT.SaveSystem
{
    public interface ISaveFileHandler
    {
        void Save(string key, string data);
        string Load (string key);

        void Delete(string key);
        
        bool Exists(string key);

    }

}

