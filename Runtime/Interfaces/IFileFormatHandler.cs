﻿namespace IMIRT.SaveSystem
{
    public interface IFileFormatHandler
    {
        string SerializeObject<T>(object dataObject);

        T DeserializeObject<T>(string dataString);
        
    }
}
