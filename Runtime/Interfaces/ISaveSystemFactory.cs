﻿namespace IMIRT.SaveSystem
{
    public interface ISaveSystemFactory
    {
        ISaveFileHandler SaveFileHandler();

        IFileFormatHandler FileFormatHandler();
    }
}