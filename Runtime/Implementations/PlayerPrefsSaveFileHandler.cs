﻿using UnityEngine;

namespace IMIRT.SaveSystem
{
    public class PlayerPrefsSaveFileHandler : ISaveFileHandler
    {
        public void Delete(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }

        public bool Exists(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public string Load(string key)
        {
            return PlayerPrefs.GetString(key);
        }

        public void Save(string key, string data)
        {
            PlayerPrefs.SetString(key, data);
        }
    }

}
